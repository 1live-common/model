package model

import "time"

type Gifts struct {
	ItemID        int        `json:"id"`
	ItemName      string     `json:"name"`
	ItemImage     string     `json:"image"`
	GoogleItemId  string     `json:"googleItemId"`
	AppleItemId   string     `json:"appleItemId"`
	Price         int        `json:"price"`
	Quantity      int        `json:"quantity"`
	Status        int        `json:"status"`
	Description   string     `json:"description"`
	AnimationUrl  string     `json:"animationUrl"`
	CreatedBy     string     `json:"createdBy"`
	CreatedAt     *time.Time `json:"createdAt"`
	LastUpdatedBy string     `json:"lastUpdatedBy"`
	LastUpdatedAt *time.Time `json:"lastUpdatedAt"`
}
