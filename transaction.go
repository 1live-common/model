package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Transaction struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	TransactionID  int                `json:"transaction_id" bson:"transaction_id"`
	ItemID         int                `json:"item_id" bson:"item_id"`
	Quantity       int                `json:"quantity" bson:"quantity"`
	Amount         int                `json:"amount" bson:"amount"`
	TotalAmount    int                `json:"total_amount" bson:"total_amount"`
	DiscountAmount int                `json:"discount_amount" bson:"discount_amount"`
	PaidAmount     int                `json:"paid_amount" bson:"paid_amount"`
	PartnerRefID   string             `json:"partner_ref_id" bson:"partner_ref_id"`
	SenderID       int                `json:"sender_id" bson:"sender_id"`
	ReceiverID     int                `json:"receiver_id" bson:"receiver_id"`
	Source         string             `json:"source" bson:"source"`
	RequestID      string             `json:"request_id" bson:"request_id"`
	Status         int                `json:"status" bson:"status"`
	TransType      int                `json:"trans_type" bson:"trans_type"`
	TransTime      int                `json:"trans_time" bson:"trans_time"`
	VideoID        int                `json:"video_id" bson:"video_id"`
	VersionNo      string             `json:"version_no" bson:"version_no"`
	CreatedIP      string             `json:"created_ip" bson:"created_ip"`
	UpdatedIP      string             `json:"updated_ip" bson:"updated_ip"`
	CreatedSource  string             `json:"created_source" bson:"created_source"`
	UpdatedSource  string             `json:"updated_source" bson:"updated_source"`
	CreatedBy      int                `json:"created_by" bson:"created_by"`
	UpdatedBy      int                `json:"updated_by" bson:"updated_by"`
	CreatedAt      *time.Time         `json:"created_at" bson:"created_at"`
	UpdatedAt      *time.Time         `json:"updated_at" bson:"updated_at"`
}

// IsExists struct
func (m Transaction) IsExists() (ok bool) {
	if m.ID.IsZero() {
		ok = false
	}
	return true
}
