package model

import (
	"time"
)

type SVideo struct {
	ID             int        `bson:"_id,omitempty" json:"_id,omitempty"`
	UploadBy       int        `bson:"upload_by" json:"upload_by"`
	ParentVideoID  int        `bson:"parent_video_id,omitempty" json:"parent_video_id,omitempty"`
	Status         int        `bson:"status" json:"status"`
	ChannelID      int        `bson:"channel_id" json:"channel_id"`
	TotalView      int        `bson:"total_view" json:"total_view"`
	View           int        `bson:"view" json:"view"`
	Share          int        `bson:"share" json:"share"`
	Price          int        `bson:"price" json:"price"`
	Like           int        `bson:"like" json:"like"`
	DisLike        int        `bson:"dislike" json:"dislike"`
	Comment        int        `bson:"comment" json:"comment"`
	Report         int        `bson:"report" json:"report"`
	Public         int        `bson:"public" json:"public"`
	Type           int        `bson:"type" json:"type"`
	Length         int        `bson:"length" json:"length"`
	Start          int        `bson:"start" json:"start"`
	End            int        `bson:"end" json:"end"`
	MaxViewer      int        `bson:"max_viewer" json:"max_viewer,omitempty"`
	TotalDonate    int        `bson:"total_donate" json:"total_donate"`
	TotalBuyTicket int        `bson:"total_buy_ticket" json:"total_buy_ticket,omitempty"`
	NewFollowers   int        `bson:"new_followers" json:"new_followers,omitempty"`
	AllowComment   bool       `bson:"allow_comment" json:"allow_comment"`
	AllowChat      bool       `bson:"allow_chat" json:"allow_chat"`
	Location       string     `bson:"location" json:"location,omitempty"`
	Password       string     `bson:"password" json:"password,omitempty"`
	HashTags       []string   `bson:"hash_tag" json:"hash_tag,omitempty"`
	Friends        []string   `bson:"friends" json:"friends,omitempty"`
	Categorises    []int      `bson:"categorises" json:"categorises,omitempty"`
	Path           string     `bson:"path" json:"path,omitempty"`
	Thumbnail      string     `bson:"thumbnail" json:"thumbnail,omitempty"`
	Description    string     `bson:"description" json:"description,omitempty"`
	MusicID        string     `bson:"music_id" json:"music_id,omitempty"`
	MusicName      string     `bson:"music_name" json:"music_name,omitempty"`
	RoomID         string     `bson:"room_id" json:"room_id,omitempty"`
	Device         string     `bson:"device" json:"device,omitempty"`
	VersionNo      string     `bson:"version_no" json:"version_no,omitempty"`
	StreamNode     string     `bson:"stream_node" json:"stream_node,omitempty"`
	SourceIP       string     `bson:"source_ip" json:"source_ip,omitempty"`
	SourceCreate   string     `bson:"source_create" json:"source_create,omitempty"`
	SourceUpdate   string     `bson:"source_update" json:"source_update,omitempty"`
	CreatedAt      *time.Time `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy      int        `bson:"created_by" json:"created_by,omitempty"`
	UpdatedAt      *time.Time `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy      int        `bson:"updated_by" json:"updated_by,omitempty"`
}

type SVideoPublic struct {
	ID            int        `bson:"_id,omitempty" json:"_id,omitempty"`
	UploadBy      int        `bson:"upload_by" json:"upload_by"`
	ParentVideoID int        `bson:"parent_video_id,omitempty" json:"parent_video_id,omitempty"`
	Status        int        `bson:"status" json:"status"`
	ChannelID     int        `bson:"channel_id" json:"channel_id"`
	View          int        `bson:"view" json:"view"`
	TotalView     int        `bson:"total_view" json:"total_view"`
	Share         int        `bson:"share" json:"share"`
	Price         int        `bson:"price" json:"price"`
	Like          int        `bson:"like" json:"like"`
	NewFollowers  int        `bson:"new_followers" json:"new_followers"`
	DisLike       int        `bson:"dislike" json:"dislike"`
	Comment       int        `bson:"comment" json:"comment"`
	Report        int        `bson:"report" json:"report"`
	Public        int        `bson:"public" json:"public"`
	Type          int        `bson:"type" json:"type"`
	Length        int        `bson:"length" json:"length"`
	Start         int        `bson:"start" json:"start"`
	End           int        `bson:"end" json:"end"`
	MaxViewer     int        `bson:"max_viewer" json:"max_viewer,omitempty"`
	AllowComment  bool       `bson:"allow_comment" json:"allow_comment"`
	AllowChat     bool       `bson:"allow_chat" json:"allow_chat"`
	HashTags      []string   `bson:"hash_tag" json:"hash_tag,omitempty"`
	Friends       []string   `bson:"friends" json:"friends,omitempty"`
	Categorises   []int      `bson:"categorises" json:"categorises,omitempty"`
	Path          string     `bson:"path" json:"path,omitempty"`
	Description   string     `bson:"description" json:"description,omitempty"`
	MusicID       string     `bson:"music_id" json:"music_id,omitempty"`
	MusicName     string     `bson:"music_name" json:"music_name,omitempty"`
	VersionNo     string     `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt     *time.Time `bson:"created_at" json:"created_at,omitempty"`
}

// IsExists struct
func (m SVideo) IsExists() (ok bool) {
	if m.ID != 0 {
		ok = true
	}
	return
}
