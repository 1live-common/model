package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserAccess struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID       int                `bson:"user_id" json:"user_id"`
	Status       int                `bson:"status" json:"status"`
	Email        string             `bson:"email" json:"email,omitempty"`
	UserName     string             `bson:"user_name" json:"user_name,omitempty"`
	PhoneNumber  string             `bson:"phone_number" json:"phone_number,omitempty"`
	Password     string             `bson:"password" json:"password,omitempty"`
	PasswordSalt string             `bson:"password_salt" json:"password_salt,omitempty"`
	AppleID      string             `bson:"apple_id" json:"apple_id,omitempty"`
	FacebookID   string             `bson:"facebook_id" json:"facebook_id,omitempty"`
	VersionNo    string             `bson:"version_no" json:"version_no,omitempty"`
	LastLoginIP  string             `bson:"last_login_ip" json:"last_login_ip,omitempty"`
	LastDevice   string             `bson:"last_device" json:"last_device,omitempty"`
	CreatedAt    *time.Time         `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy    int                `bson:"created_by" json:"created_by,omitempty"`
	UpdatedAt    *time.Time         `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy    int                `bson:"updated_by" json:"updated_by,omitempty"`
}

// IsExists struct
func (m UserAccess) IsExists() (ok bool) {
	if m.UserID != 0 {
		ok = true
	}
	return
}
