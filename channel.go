package model

import (
	"time"
)

type Channel struct {
	ChannelID           int        `bson:"_id,omitempty" json:"_id,omitempty"`
	ChannelName         string     `bson:"channel_name" json:"channel_name"`
	CoverImage          string     `bson:"cover_image" json:"cover_image"`
	Categories          []int      `bson:"categories" json:"categories"`
	Status              int        `bson:"status" json:"status"`
	UserID              int        `bson:"user_id" json:"user_id"`
	CurrentLiveStreamID int        `bson:"current_live_stream_id" json:"current_live_stream_id"`
	StreamKey           string     `bson:"stream_key" json:"stream_key"`
	OnStreaming         bool       `bson:"on_streaming" json:"on_streaming"`
	Description         string     `bson:"description" json:"description,omitempty"`
	VersionNo           string     `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt           *time.Time `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy           int        `bson:"created_by" json:"created_by,omitempty"`
	UpdatedAt           *time.Time `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy           int        `bson:"updated_by" json:"updated_by,omitempty"`
}

// IsExists struct
func (m Channel) IsExists() (ok bool) {
	if m.ChannelID != 0 {
		ok = true
	}
	return
}
