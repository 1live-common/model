package model

import (
	"time"
)

type VCategory struct {
	ID          int        `bson:"_id,omitempty" json:"_id,omitempty"`
	Name        string     `bson:"name" json:"name"`
	Icon        string     `bson:"icon" json:"icon"`
	Status      int        `bson:"status" json:"status"`
	Description string     `bson:"description" json:"description,omitempty"`
	VersionNo   string     `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt   *time.Time `bson:"created_at" json:"created_at,omitempty"`
}

// IsExists struct
func (m VCategory) IsExists() (ok bool) {
	if m.ID != 0 {
		ok = true
	}
	return
}
