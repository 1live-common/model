package model

import (
	"time"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Wallet struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID      int                `json:"user_id" bson:"user_id"`
	Coin        int                `json:"coin" bson:"coin"`
	Promotion   int                `json:"promotion" bson:"promotion"`
	Point       int                `json:"point" bson:"point"`
	TotalIn     int                `json:"total_in,omitempty" bson:"total_in,omitempty"`
	TotalTopUp  int                `json:"total_top_up,omitempty" bson:"total_top_up,omitempty"`
	TotalOut    int                `json:"total_out,omitempty" bson:"total_out,omitempty"`
	MaxTopUp    int                `json:"max_top_up,omitempty" bson:"max_top_up,omitempty"`
	MinTopUp    int                `json:"min_top_up,omitempty" bson:"min_top_up,omitempty"`
	MaxTransfer int                `json:"max_transfer,omitempty" bson:"max_transfer,omitempty"`
	Status      int                `json:"status" bson:"status"`
	LastTransID string             `bson:"last_trans_id" json:"last_trans_id,omitempty"`
	VersionNo   string             `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt   *time.Time         `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy   int                `bson:"created_by" json:"created_by,omitempty"`
	UpdatedAt   *time.Time         `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy   int                `bson:"updated_by" json:"updated_by,omitempty"`
}

// IsExists struct
func (m Wallet) IsExists() (ok bool) {
	if !m.ID.IsZero() {
		ok = true
	}
	return
}
