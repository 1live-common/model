package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type VideoReport struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID      int                `bson:"user_id" json:"user_id,omitempty"`
	VideoID     int                `bson:"video_id" json:"video_id,omitempty"`
	Description string             `bson:"description" json:"description,omitempty"`
	Source      string             `bson:"source" json:"source,omitempty"`
	Visible     bool               `bson:"visible" json:"visible"`
	CreateIP    string             `bson:"create_ip" json:"create_ip,omitempty"`
	VersionNo   string             `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt   *time.Time         `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy   int                `bson:"created_by" json:"created_by,omitempty"`
	UserAgent   string             `bson:"user_agent" json:"user_agent,omitempty"`
}

// IsExists struct
func (m VideoReport) IsExists() (ok bool) {
	if m.VideoID != 0 {
		ok = true
	}
	return
}
