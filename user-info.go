package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ShowUserInfo struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID      int                `bson:"user_id" json:"user_id"`
	UserName    string             `bson:"user_name" json:"user_name,omitempty"`
	Type        string             `bson:"type" json:"type,omitempty"` // View, Live, Moderator
	Gender      int                `bson:"gender,omitempty" json:"gender"`
	Bio         string             `bson:"bio" json:"bio"`
	Email       string             `bson:"email" json:"email"`
	Hobbies     []int              `bson:"hobbies" json:"hobbies"`
	PhoneNumber string             `bson:"phone_number" json:"phone_number"`
	DisplayName string             `bson:"display_name" json:"display_name"`
	Avatar      string             `bson:"avatar_link" json:"avatar_link"`
	Cover       string             `bson:"cover_image" json:"cover_image"`
	ChatID      string             `bson:"chat_id" json:"chat_id,omitempty"`
	Status      int                `bson:"status" json:"status"`
	Verified    bool               `bson:"verified" json:"verified"`
}

type UserInfo struct {
	ID                 primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID             int                `bson:"user_id" json:"user_id"`
	UserName           string             `bson:"user_name" json:"user_name,omitempty"`
	Type               string             `bson:"type" json:"type,omitempty"` // View, Live, Moderator
	Gender             int                `bson:"gender" json:"gender"`
	Email              string             `bson:"email" json:"email"`
	PhoneNumber        string             `bson:"phone_number" json:"phone_number"`
	DisplayName        string             `bson:"display_name" json:"display_name"`
	YearOfBirth        int                `bson:"year_of_birth" json:"year_of_birth"`
	DateOfBirth        *time.Time         `bson:"date_of_birth" json:"date_of_birth,omitempty"`
	Avatar             string             `bson:"avatar_link" json:"avatar_link"`
	Cover              string             `bson:"cover_image" json:"cover_image"`
	Bio                string             `bson:"bio" json:"bio"`
	Hobbies            []int              `bson:"hobbies" json:"hobbies"`
	PersonalCardAhead  string             `bson:"personal_card_ahead" json:"personal_card_ahead"`
	PersonalCardBehind string             `bson:"personal_card_behind" json:"personal_card_behind"`
	PersonalID         string             `bson:"personal_id" json:"personal_id"`
	Relationship       string             `bson:"relationship" json:"relationship"`
	AppleID            string             `bson:"apple_id" json:"apple_id,omitempty"`
	FacebookID         string             `bson:"facebook_id" json:"facebook_id,omitempty"`
	FacebookProfile    string             `bson:"facebook_profile" json:"facebook_profile,omitempty"`
	InstagramProfile   string             `bson:"instagram_profile" json:"instagram_profile,omitempty"`
	ChatID             string             `bson:"chat_id" json:"chat_id,omitempty"`
	ChatToken          string             `bson:"chat_token" json:"chat_token,omitempty"`
	Language           string             `bson:"language" json:"language"`
	VersionNo          string             `bson:"version_no" json:"version_no"`
	Status             int                `bson:"status" json:"status"`
	RegisterSource     string             `bson:"register_source" json:"register_source,omitempty"`
	Verified           bool               `bson:"verified" json:"verified"`
	CreatedAt          *time.Time         `bson:"created_at,omitempty" json:"created_at,omitempty"`
	CreatedBy          int                `bson:"created_by,omitempty" json:"created_by,omitempty"`
	UpdatedAt          *time.Time         `bson:"updated_at,omitempty" json:"updated_at,omitempty"`
	UpdatedBy          int                `bson:"updated_by,omitempty" json:"updated_by,omitempty"`
	UpdatedSource      string             `bson:"updated_source,omitempty" json:"updated_source,omitempty"`
	UpdatedSourceIP    string             `bson:"updated_source_ip,omitempty" json:"updated_source_ip,omitempty"`
}

// IsExists struct
func (m UserInfo) IsExists() (ok bool) {
	if m.UserID != 0 {
		ok = true
	}
	return
}

// IsExists struct
func (m ShowUserInfo) IsExists() (ok bool) {
	if m.UserID != 0 {
		ok = true
	}
	return
}
