package model

import (
	"time"
)

type GroupCoin struct {
	ID              int        `bson:"_id,omitempty" json:"_id,omitempty"`
	GroupName       string     `json:"coin_name" bson:"image,omitempty"`
	GroupImage      string     `json:"image" bson:"image,omitempty"`
	GroupImageHover string     `json:"image_hover" bson:"image_hover,omitempty"`
	Description     string     `json:"description,omitempty" bson:"description,omitempty"` // description
	Quantity        int        `json:"quantity" bson:"quantity"`
	Price           int        `json:"price" bson:"price"`
	Sort            int        `json:"sort" bson:"sort"`
	Status          int        `json:"status" bson:"status"`
	CreatedBy       string     `json:"created_by" bson:"created_by"`
	CreatedAt       *time.Time `json:"created_at" bson:"created_at"`
	UpdatedBy       string     `json:"updated_by" bson:"updated_by,omitempty"`
	UpdatedAt       *time.Time `json:"updated_at" bson:"updated_at,omitempty"`
}