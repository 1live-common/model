package model

import "github.com/dgrijalva/jwt-go"

type TokenClaims struct {
	LoginRequestID string
	UserID int
	UserAgent string
	Source string
	LoginTime int
	jwt.StandardClaims
}