package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Items struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	ItemID       int                `json:"item_id" bson:"item_id,omitempty"`
	ItemName     string             `json:"item_name" bson:"image,omitempty"`
	ItemImage    string             `json:"image" bson:"image,omitempty"`
	GoogleItemId string             `json:"google_item_id,omitempty" bson:"google_item_id,omitempty"`
	AppleItemId  string             `json:"apple_item_id,omitempty" bson:"apple_item_id,omitempty"`
	Description  string             `json:"description,omitempty" bson:"description,omitempty"` // description
	Price        int                `json:"price" bson:"price"`
	Status       int                `json:"status" bson:"status"`
	CreatedBy    string             `json:"created_by" bson:"created_by"`
	CreatedAt    *time.Time         `json:"created_at" bson:"created_at"`
	UpdatedBy    string             `json:"updated_by" bson:"updated_by,omitempty"`
	UpdatedAt    *time.Time         `json:"updated_at" bson:"updated_at,omitempty"`
}
