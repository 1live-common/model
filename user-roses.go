package model

import "time"

type UserItems struct {
	ID            int        `json:"_id" bson:"_id"`
	TransactionID int        `json:"transaction_id" bson:"transaction_id"`
	ItemID        int        `json:"item_id" bson:"item_id"`
	Quantity      int        `json:"quantity" bson:"quantity"`
	UserID        int        `json:"user_id" bson:"user_id"`
	Status        int        `json:"status" bson:"status"`
	TranType      int        `json:"tran_type" bson:"tran_type"`
	VersionNo     string     `json:"version_no" bson:"version_no"`
	CreatedBy     int        `json:"created_by" bson:"created_by"`
	UpdatedBy     int        `json:"updated_by" bson:"updated_by"`
	CreatedAt     *time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt     *time.Time `json:"updated_at" bson:"updated_at"`
}
