package model

type MongoStreamObj struct {
	ID            interface{} `json:"_id"`
	Data          interface{} `json:"data"`
	Type          string      `json:"type"`
	UpdatedFields interface{} `json:"updated_fields"`
}
