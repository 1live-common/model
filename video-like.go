package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type VideoLike struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID    int                `bson:"user_id" json:"user_id,omitempty"`
	VideoID   int                `bson:"video_id" json:"video_id,omitempty"`
	Status    int                `bson:"status" json:"status"`
	Type      int                `bson:"type" json:"type"`
	VersionNo string             `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt *time.Time         `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy int                `bson:"created_by" json:"created_by,omitempty"`
}

// IsExists struct
func (m VideoLike) IsExists() (ok bool) {
	if m.VideoID != 0 {
		ok = true
	}
	return
}
