package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type DeviceToken struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID    int                `bson:"user_id" json:"user_id"`
	Source    string             `bson:"source,omitempty" json:"source,omitempty"`
	Token     string             `bson:"token" json:"token"`
	Visible   bool               `bson:"visible" json:"visible"`
	VersionNo string             `bson:"version_no" json:"version_no"`
	CreatedAt *time.Time         `bson:"created_at" json:"created_at"`
	UpdatedAt *time.Time         `bson:"updated_at" json:"updated_at"`
}