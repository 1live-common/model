package model

import "time"

type StreamNode struct {
	NodeID       int        `json:"_id" bson:"_id"`
	SourceIP     string     `json:"source_ip" bson:"source_ip"`
	Command      string     `json:"command" bson:"command"`
	TotalSession int        `json:"total_session" bson:"total_session"`
	MaxSession   int        `json:"max_session" bson:"max_session"`
	Visible      bool       `json:"visible" bson:"visible"`
	Status       int        `json:"status" bson:"status"`
	VersionNo    string     `json:"version_no" bson:"version_no"`
	CreatedBy    int        `json:"created_by" bson:"created_by"`
	CreatedAt    *time.Time `json:"created_at" bson:"created_at"`
	UpdatedBy    int        `json:"updated_by" bson:"updated_by"`
	UpdatedAt    *time.Time `json:"updated_at" bson:"updated_at"`
}
