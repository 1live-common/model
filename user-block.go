package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserBlock struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID    int                `bson:"user_id" json:"user_id,omitempty"`
	Blocked   int                `bson:"blocked" json:"blocked,omitempty"`
	Visible   bool               `bson:"visible" json:"visible"`
	VersionNo string             `bson:"version_no" json:"version_no,omitempty"`
	CreatedAt *time.Time         `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy int                `bson:"created_by" json:"created_by,omitempty"`
}

// IsExists struct
func (m UserBlock) IsExists() (ok bool) {
	if m.UserID != 0 {
		ok = true
	}
	return
}
