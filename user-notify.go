package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserNotify struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	UserID    int                `bson:"user_id" json:"user_id"`
	App       string             `bson:"app" json:"app"`
	Title     string             `bson:"title" json:"title"`
	Body      string             `bson:"body" json:"body"`
	Payload   string             `bson:"payload" json:"payload"`
	Icon      string             `bson:"icon" json:"icon"`
	Type      string             `bson:"type" json:"type"`
	Target    string             `bson:"target" json:"target"`
	IsOpen    bool               `bson:"is_open" json:"is_open"`
	Visible   bool               `bson:"visible" json:"visible"`
	VersionNo string             `bson:"version_no" json:"version_no"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at"`
}
