package model

type Sticker struct {
	ID         int    `bson:"_id,omitempty" json:"_id,omitempty"`
	Icon       string `bson:"icon" json:"icon"`
	Url        string `bson:"url" json:"url"`
	Preview    string `bson:"preview" json:"preview"`
	Name       string `bson:"name" json:"name"`
	Duration   int    `bson:"duration" json:"duration"`
	Desc       string `bson:"desc" json:"desc"`
	Aspect     int    `bson:"aspect" json:"aspect"`
	Sort       int    `bson:"sort" json:"sort"`
	Type       int    `bson:"type" json:"type"`
	FontId     int    `bson:"font_id" json:"font_id"`
	CreateTime string `bson:"create_time" json:"create_time"`
}
